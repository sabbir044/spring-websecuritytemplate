package com.sabbir.websecuriy.Configs;

import com.sabbir.websecuriy.entity.UserPasswordEntity;
import com.sabbir.websecuriy.model.User;
import com.sabbir.websecuriy.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class InitLoader implements CommandLineRunner {
    private static final Logger logger = Logger.getLogger(InitLoader.class.getName());
    @Autowired
    private UserService userService;

    @Override
    public void run(String... args) throws Exception {
        final UserPasswordEntity userPasswordEntity = UserPasswordEntity.of("sabbir044@gmail.com", "123456", User.Role.ADMIN);
        final User user = userService.createUser(userPasswordEntity);
        logger.info(user.toString());

        final UserPasswordEntity userPasswordEntity2 = UserPasswordEntity.of("sabbir@gmail.com", "123456", User.Role.USER);
        final User user2 = userService.createUser(userPasswordEntity2);
        logger.info(user2.toString());
    }
}
