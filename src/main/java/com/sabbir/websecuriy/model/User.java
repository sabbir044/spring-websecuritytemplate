package com.sabbir.websecuriy.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "User")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(unique = true, nullable = false)
    private String email;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role = Role.ADMIN;

    public static enum Role {
        ADMIN("ADMIN"),
        USER("USER");

        private final String roleName;

        private Role(String roleString) {
            this.roleName = roleString;
        }

        public static Role getRoleByRoleName(String roleName) {
            switch (roleName) {
                case "ADMIN":
                    return ADMIN;
                case "USER":
                    return USER;
                default:
                    return null;
            }
        }

        @Override
        public String toString() {
            return roleName;
        }
    }
}
