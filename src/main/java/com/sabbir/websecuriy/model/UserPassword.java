package com.sabbir.websecuriy.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "UserPassword")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserPassword {
    @Id
    private Long userId;
    private String password;
}
