package com.sabbir.websecuriy.services;

import com.sabbir.websecuriy.entity.UserPasswordEntity;
import com.sabbir.websecuriy.model.User;
import com.sabbir.websecuriy.model.UserPassword;
import com.sabbir.websecuriy.model.repository.UserPasswordRepository;
import com.sabbir.websecuriy.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserPasswordRepository userPasswordRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public User createUser(UserPasswordEntity userPasswordEntity) {
        User user = new User();
        user.setEmail(userPasswordEntity.getEmail());
        user.setRole(userPasswordEntity.getRole());
        final User newUser = userRepository.save(user);
        UserPassword userPassword = new UserPassword();
        userPassword.setUserId(newUser.getId());
        userPassword.setPassword(passwordEncoder.encode(userPasswordEntity.getPassword()));
        userPasswordRepository.save(userPassword);
        return newUser;
    }
}
