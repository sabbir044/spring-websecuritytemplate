package com.sabbir.websecuriy.entity;

import com.sabbir.websecuriy.model.User;
import lombok.Data;
import lombok.NonNull;

@Data(staticConstructor = "of")
public class UserPasswordEntity {
    @NonNull
    private String email;
    @NonNull
    private String password;
    @NonNull
    private User.Role role;
}
